/**
 * File EstateMapActivity.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.activities;

import org.holoeverywhere.app.Activity;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.dialogs.DialogFactory;
import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/**
 * The Class EstateMapActivity.
 * 
 * @author Stelian Morariu
 */
public class EstateMapActivity extends Activity {

    /*
     * (non-Javadoc)
     * @see org.holoeverywhere.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_estate_map);
    }

    /*
     * (non-Javadoc)
     * @see android.support.v4.app._HoloActivity#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	getSupportMenuInflater().inflate(R.menu.activity_estate_map, menu);
	return true;
    }

    /*
     * (non-Javadoc)
     * @see android.support.v4.app._HoloActivity#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	if (item.getItemId() == R.id.menu_tasks) {
	    startActivity(new Intent(this, TasksActivity.class));
	    return true;
	} else if (item.getItemId() == R.id.menu_about) {
	    DialogFactory.showAboutAlertDialog(this);
	    return true;
	} else
	    return super.onOptionsItemSelected(item);
    }

}

/**
 * File TasksActivity.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.activities;

import org.holoeverywhere.app.Activity;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.fragments.TasksFragment;
import android.os.Bundle;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/**
 * The Class TasksActivity.
 * 
 * @author Stelian Morariu
 */
public class TasksActivity extends Activity {

    private TasksFragment tasksFragment;

    /*
     * (non-Javadoc)
     * @see org.holoeverywhere.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	setContentView(R.layout.activity_tasks);

	this.tasksFragment = (TasksFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentTasks);

	getSupportActionBar().setHomeButtonEnabled(true);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /*
     * (non-Javadoc)
     * @see android.support.v4.app._HoloActivity#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	getSupportMenuInflater().inflate(R.menu.activity_tasks_menu, menu);

	if (this.tasksFragment != null) {
	    this.tasksFragment.setRefreshMenuItem(menu.findItem(R.id.menu_refresh));
	}
	return true;
    }

    /*
     * (non-Javadoc)
     * @see android.support.v4.app._HoloActivity#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	if (item.getItemId() == android.R.id.home) {
	    finish();
	    return true;
	} else if (item.getItemId() == R.id.menu_refresh) {
	    if (this.tasksFragment != null) {
		this.tasksFragment.refresh();
	    }
	    return true;
	} else {
	    return super.onOptionsItemSelected(item);
	}
    }

}

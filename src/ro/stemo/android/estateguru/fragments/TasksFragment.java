/**
 * File TasksFragment.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;

import com.actionbarsherlock.view.MenuItem;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.adapters.TasksAdapter;
import ro.stemo.android.estateguru.dialogs.DialogFactory;
import ro.stemo.android.estateguru.model.TaskModel;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * The Class TasksFragment.
 * 
 * @author Stelian Morariu
 */
public class TasksFragment extends Fragment implements OnItemClickListener {

    private static final String TASK_URL = "http://defacut.herokuapp.com/items.json";

    private ListView taskList;

    private TasksAdapter tasksAdapter;

    private MenuItem refreshMenuItem;

    private boolean initialized = false;

    private class AsyncTaskReader extends AsyncTask<Void, Void, String> {

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
	    if (refreshMenuItem != null) {
		refreshMenuItem.setActionView(R.layout.refresh_menu_hack);
	    }
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected String doInBackground(Void... params) {
	    final HttpGet getRequest = new HttpGet(TASK_URL);
	    final DefaultHttpClient client = new DefaultHttpClient();
	    String result = null;
	    try {
		final HttpResponse response = client.execute(getRequest);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK && response.getEntity() != null) {
		    result = EntityUtils.toString(response.getEntity());
		}
	    } catch (Exception e) {
		getRequest.abort();
	    }

	    return result;
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(String result) {
	    if (refreshMenuItem != null) {
		refreshMenuItem.setActionView(null);
	    }

	    try {
		JSONArray array = new JSONArray(result);
		List<TaskModel> tasks = new ArrayList<TaskModel>();

		for (int i = 0; i < array.length(); i++) {
		    tasks.add(new TaskModel(array.getJSONObject(i)));
		}

		tasksAdapter = new TasksAdapter(getActivity(), tasks);
		taskList.setAdapter(tasksAdapter);
		taskList.refreshDrawableState();
		initialized = true;

	    } catch (JSONException e) {
		Log.e(AsyncTaskReader.class.getSimpleName(), e.toString());
	    }
	}
    }

    /*
     * (non-Javadoc)
     * @see android.support.v4.app._HoloFragment#onCreateView(org.holoeverywhere.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View mView = inflater.inflate(R.layout.fragment_tasks, container, false);

	this.taskList = (ListView) mView.findViewById(R.id.taskList);
	this.taskList.setEmptyView(mView.findViewById(R.id.emptyView));
	this.taskList.setOnItemClickListener(this);

	return mView;
    }

    /*
     * (non-Javadoc)
     * @see org.holoeverywhere.widget.AdapterView.OnItemClickListener#onItemClick(org.holoeverywhere.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	TaskModel mdl = this.tasksAdapter.getItem(position);
	DialogFactory.showTaskStatusDialog((Activity) getActivity(), mdl);
    }

    /**
     * Refresh the list of tasks.
     */
    public void refresh() {
	new AsyncTaskReader().execute();
    }

    /**
     * Sets the refresh menu item reference.
     * 
     * @param item
     *            the refresh menu item
     */
    public void setRefreshMenuItem(MenuItem item) {
	this.refreshMenuItem = item;
	if (!this.initialized) {
	    refresh();
	}
    }

}

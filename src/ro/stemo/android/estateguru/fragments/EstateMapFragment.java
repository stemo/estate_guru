/**
 * File EstateMapFragment.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.fragments;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.adapters.EstateInfoWindowAdapter;
import ro.stemo.android.estateguru.model.EstateModel;
import ro.stemo.android.estateguru.persistence.DBHandler;
import ro.stemo.android.estateguru.persistence.PreferedEstate;
import ro.stemo.android.estateguru.utils.SherlockMapFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * The Class EstateMapFragment.
 * 
 * @author Stelian Morariu
 */
public class EstateMapFragment extends SherlockMapFragment implements OnGlobalLayoutListener, OnInfoWindowClickListener, OnMarkerClickListener {

    private GoogleMap mMap;

    private boolean initialised = false;

    private List<EstateModel> allHouses = new ArrayList<EstateModel>();

    private List<PreferedEstate> preferedHouses = new ArrayList<PreferedEstate>();

    private List<Marker> mapMarkers = new ArrayList<Marker>();

    private EstateInfoWindowAdapter estateInfoWindowAdatper;

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.SupportMapFragment#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	readLocations();
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.SupportMapFragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View root = super.onCreateView(inflater, container, savedInstanceState);
	root.getViewTreeObserver().addOnGlobalLayoutListener(this);

	this.estateInfoWindowAdatper = new EstateInfoWindowAdapter(getActivity());

	this.mMap = getMap();
	if (this.mMap != null) {
	    this.mMap.setInfoWindowAdapter(this.estateInfoWindowAdatper);
	    this.mMap.setOnInfoWindowClickListener(this);
	    this.mMap.setOnMarkerClickListener(this);

	    addMarkers();
	}

	return root;
    }

    /*
     * (non-Javadoc)
     * @see android.view.ViewTreeObserver.OnGlobalLayoutListener#onGlobalLayout()
     */
    @Override
    public void onGlobalLayout() {
	if (this.mMap != null && !this.initialised) {
	    LatLngBounds.Builder bld = new LatLngBounds.Builder();
	    for (Marker mkr : this.mapMarkers) {
		bld.include(mkr.getPosition());
	    }
	    LatLngBounds bounds = bld.build();
	    this.mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 96));
	    this.initialised = true;
	}
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.GoogleMap.OnMarkerClickListener#onMarkerClick(com.google.android.gms.maps.model.Marker)
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
	if (this.estateInfoWindowAdatper != null) {
	    this.estateInfoWindowAdatper.updateEstateModel(getModelForMarker(marker));
	}

	// let the map animate itself to the selected marker
	return false;
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener#onInfoWindowClick(com.google.android.gms.maps.model.Marker)
     */
    @Override
    public void onInfoWindowClick(Marker marker) {
	marker.hideInfoWindow();

	EstateModel model = getModelForMarker(marker);

	if (model != null) {
	    if (model.isPrefered()) {
		final PreferedEstate preferedEstate = getPreferedEstate(model.getEstateCode());
		if (preferedEstate != null) {
		    boolean deleted = DBHandler.getInstance(getActivity()).removePreferedEstate(preferedEstate);
		    if (deleted) {
			model.setPrefered(false);
			this.preferedHouses.remove(preferedEstate);
		    }
		}
	    } else {
		PreferedEstate pfe = new PreferedEstate();
		pfe.setEstateCode(model.getEstateCode());

		boolean inserted = DBHandler.getInstance(getActivity()).savePreferedEstate(pfe);
		if (inserted) {
		    model.setPrefered(true);
		    this.preferedHouses.add(pfe);
		}
	    }

	    // remove old marker and add a new one to reflect changes
	    marker.remove();

	    BitmapDescriptor markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.location_green_normal);
	    if (model.isPrefered()) {
		markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.location_green_preferred);
	    }

	    Marker newMarker = this.mMap.addMarker(new MarkerOptions().position(new LatLng(model.getLatitude(), model.getLongitude())).title(model.getEstateCode()).icon(markerIcon));
	    newMarker.showInfoWindow();
	}

    }

    private PreferedEstate getPreferedEstate(String estateCode) {
	PreferedEstate pfe = null;
	for (PreferedEstate p : this.preferedHouses) {
	    if (p.getEstateCode().equalsIgnoreCase(estateCode)) {
		pfe = p;
		break;
	    }
	}

	return pfe;
    }

    private EstateModel getModelForMarker(Marker marker) {
	final String title = marker.getTitle();
	EstateModel model = null;

	for (EstateModel mdl : this.allHouses) {

	    if (title.equalsIgnoreCase(mdl.getEstateCode())) {
		model = mdl;
		break;
	    }
	}

	return model;
    }

    private void readLocations() {
	this.allHouses.clear();
	this.preferedHouses.clear();
	this.mapMarkers.clear();

	this.preferedHouses.addAll(DBHandler.getInstance(getActivity()).getPreferedestates());

	InputStream io = getResources().openRawResource(R.raw.houses);

	try {
	    byte[] b = new byte[io.available()];
	    io.read(b);

	    String json = new String(b);

	    JSONObject obj = new JSONObject(json);
	    JSONArray array = obj.getJSONArray("houses");

	    for (int i = 0; i < array.length(); i++) {
		JSONObject house = array.getJSONObject(i);
		final EstateModel estateModel = new EstateModel(house);
		estateModel.setPrefered(isHousePreffered(estateModel));
		this.allHouses.add(estateModel);

	    }

	} catch (Exception e) {
	    Log.e(EstateMapFragment.class.getSimpleName(), e.toString());
	}

    }

    private boolean isHousePreffered(EstateModel house) {
	boolean result = false;

	for (PreferedEstate pfe : this.preferedHouses) {
	    if (pfe.getEstateCode().equalsIgnoreCase(house.getEstateCode())) {
		result = true;
		break;
	    }
	}

	return result;
    }

    private void addMarkers() {
	for (EstateModel model : this.allHouses) {
	    BitmapDescriptor markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.location_green_normal);

	    if (model.isPrefered()) {
		markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.location_green_preferred);
	    }

	    final Marker marker = this.mMap.addMarker(new MarkerOptions().position(new LatLng(model.getLatitude(), model.getLongitude())).title(model.getEstateCode()).icon(markerIcon));
	    this.mapMarkers.add(marker);
	}
    }

}

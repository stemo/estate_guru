package ro.stemo.android.estateguru.utils;

/**
 * File SherlockMapFragment.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import android.support.v4.app.Watson.OnCreateOptionsMenuListener;
import android.support.v4.app.Watson.OnOptionsItemSelectedListener;
import android.support.v4.app.Watson.OnPrepareOptionsMenuListener;

import com.actionbarsherlock.internal.view.menu.MenuItemWrapper;
import com.actionbarsherlock.internal.view.menu.MenuWrapper;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * The Class SherlockMapFragment.
 */
public class SherlockMapFragment extends SupportMapFragment implements OnCreateOptionsMenuListener, OnPrepareOptionsMenuListener, OnOptionsItemSelectedListener {
    private org.holoeverywhere.app.Activity mActivity;

    public org.holoeverywhere.app.Activity getSherlockActivity() {
	return mActivity;
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.SupportMapFragment#onAttach(android.app.Activity)
     */
    @Override
    public void onAttach(android.app.Activity activity) {
	if (!(activity instanceof org.holoeverywhere.app.Activity)) {
	    throw new IllegalStateException(getClass().getSimpleName() + " must be attached to a Holoeverywhere activity.");
	}
	mActivity = (org.holoeverywhere.app.Activity) activity;

	super.onAttach(activity);
    }

    @Override
    public void onDetach() {
	mActivity = null;
	super.onDetach();
    }

    @Override
    public final void onCreateOptionsMenu(android.view.Menu menu, android.view.MenuInflater inflater) {
	onCreateOptionsMenu(new MenuWrapper(menu), mActivity.getSupportMenuInflater());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// Nothing to see here.
    }

    @Override
    public final void onPrepareOptionsMenu(android.view.Menu menu) {
	onPrepareOptionsMenu(new MenuWrapper(menu));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
	// Nothing to see here.
    }

    @Override
    public final boolean onOptionsItemSelected(android.view.MenuItem item) {
	return onOptionsItemSelected(new MenuItemWrapper(item));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Nothing to see here.
	return false;
    }
}
/**
 * File DBHandler.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.persistence;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The Class DBHandler.
 * 
 * @author Stelian Morariu
 */
public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "houses";

    private static DBHandler instance;

    /**
     * Gets the single instance of DBHandler.
     * 
     * @param context
     *            the context
     * @return single instance of DBHandler
     */
    public static DBHandler getInstance(Context context) {
	if (instance == null) {
	    instance = new DBHandler(context);
	}

	return instance;
    }

    private DBHandler(Context context) {
	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
     * (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
	db.execSQL(PreferedEstate.CREATE_TABLE);
    }

    /*
     * (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * Returns a list with all the prefered estates.
     * 
     * @return the prefered estates
     */
    public synchronized List<PreferedEstate> getPreferedestates() {
	final SQLiteDatabase db = this.getReadableDatabase();
	final Cursor cursor = db.query(PreferedEstate.TABLE, PreferedEstate.FIELDS, null, null, null, null, null, null);

	List<PreferedEstate> estates = new ArrayList<PreferedEstate>();
	if (cursor.moveToFirst()) {
	    while (cursor.moveToNext()) {
		estates.add(new PreferedEstate(cursor));
	    }
	}
	cursor.close();
	return estates;
    }

    /**
     * Save prefered estate.
     * 
     * @param estate
     *            the estate
     * @return true, if successful
     */
    public synchronized boolean savePreferedEstate(final PreferedEstate estate) {
	boolean success = false;
	final SQLiteDatabase db = this.getWritableDatabase();

	final long id = db.insert(PreferedEstate.TABLE, null, estate.getContent());

	if (id > -1) {
	    estate.setId((int) id);
	    success = true;
	}

	return success;
    }

    /**
     * Removes the prefered estate.
     * 
     * @param estate
     *            the estate
     * @return the int
     */
    public synchronized boolean removePreferedEstate(final PreferedEstate estate) {
	final SQLiteDatabase db = this.getWritableDatabase();
	final int result = db.delete(PreferedEstate.TABLE, PreferedEstate._ID + " IS ?", new String[] { String.valueOf(estate.getId()) });

	return result > 0;
    }

}

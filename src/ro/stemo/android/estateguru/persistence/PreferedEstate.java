/**
 * File PreferedEstate.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.persistence;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * The Class PreferedEstate.
 */
public class PreferedEstate {

    /** The Constant TABLE. */
    public static final String TABLE = "Prefered";

    /** The Constant _ID. */
    public static final String _ID = "_id";

    /** The Constant ESTATE_CODE. */
    public static final String ESTATE_CODE = "estate_code";

    // For database projection so order is consistent
    /** The Constant FIELDS. */
    public static final String[] FIELDS = { _ID, ESTATE_CODE };

    /** The Constant CREATE_TABLE. */
    public static final String CREATE_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY, %s TEXT NOT NULL DEFAULT '')", TABLE, _ID, ESTATE_CODE);

    private int id;

    private String estateCode;

    /**
     * Instantiates a new prefered estate.
     */
    public PreferedEstate() {
	this.id = -1;
	this.estateCode = "";
    }

    /**
     * Instantiates a new prefered estate.
     * 
     * @param cursor
     *            the cursor
     */
    public PreferedEstate(final Cursor cursor) {
	this.id = cursor.getInt(0);
	this.estateCode = cursor.getString(1);

    }

    /**
     * Gets the content.
     * 
     * @return the content
     */
    public ContentValues getContent() {
	final ContentValues values = new ContentValues();
	// Note that ID is NOT included here
	values.put(ESTATE_CODE, this.estateCode);

	return values;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getEstateCode() {
	return estateCode;
    }

    public void setEstateCode(String estateCode) {
	this.estateCode = estateCode;
    }

}

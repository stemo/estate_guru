/**
 * File EstateModel.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.model;

import org.json.JSONObject;

import android.util.Log;

/**
 * The Class EstateModel.
 */
public class EstateModel {

    private String owner;

    private String street;

    private String city;

    private String country;

    private String type;

    private String code;

    private String created;

    private String dueDate;

    private int number;

    private double latitude;

    private double longitude;

    private double price;

    private boolean prefered;

    /**
     * Instantiates a new estate model.
     */
    public EstateModel() {
	this.owner = "";
	this.street = "";
	this.city = "";
	this.country = "";
	this.type = "";
	this.code = "";
	this.created = "";
	this.dueDate = "";
	this.number = 0;
	this.latitude = 0;
	this.longitude = 0;
	this.price = 0;
	this.prefered = false;
    }

    /**
     * Instantiates a new estate model.
     * 
     * @param house
     *            the house
     */
    public EstateModel(JSONObject house) {
	this();
	try {
	    this.owner = house.getString("owner");
	    this.street = house.getString("street");
	    this.city = house.getString("city");
	    this.country = house.getString("country");
	    this.type = house.getString("type");
	    this.code = house.getString("code");
	    this.created = house.getString("created");
	    this.dueDate = house.getString("due_date");
	    this.number = house.getInt("number");
	    this.latitude = house.getDouble("latitude");
	    this.longitude = house.getDouble("longitude");
	    this.price = house.getDouble("price");
	    this.prefered = false;
	} catch (Exception e) {
	    Log.e(EstateModel.class.getSimpleName(), e.toString());
	}
    }

    public String getOwner() {
	return owner;
    }

    public void setOwner(String owner) {
	this.owner = owner;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getCreated() {
	return created;
    }

    public void setCreated(String created) {
	this.created = created;
    }

    public String getDueDate() {
	return dueDate;
    }

    public void setDueDate(String dueDate) {
	this.dueDate = dueDate;
    }

    public int getNumber() {
	return number;
    }

    public void setNumber(int number) {
	this.number = number;
    }

    public double getLatitude() {
	return latitude;
    }

    public void setLatitude(double latitude) {
	this.latitude = latitude;
    }

    public double getLongitude() {
	return longitude;
    }

    public void setLongitude(double longitude) {
	this.longitude = longitude;
    }

    public double getPrice() {
	return price;
    }

    public void setPrice(double price) {
	this.price = price;
    }

    public boolean isPrefered() {
	return prefered;
    }

    public void setPrefered(boolean prefered) {
	this.prefered = prefered;
    }

    /**
     * Return a unique code identifying this house. Different from {@link getCode()} because {@code code} is not unique.
     * <p>
     * The process of generating a unique id for every house is the following :
     * <ul>
     * <li>Create a new identifier by concatenating the {@code owner,street} and {@code number} of the house;</li>
     * <li>get the {@code hasCode()} of the newly created identifier and transform it to a string;</li>
     * <li>append the identifier hash code to the {@code code} property.</li>
     * </ul>
     * This approach is based on the fact that 2 String objects can have the same {@code hashCode()} only if they are identical.
     */
    public String getEstateCode() {
	String unique = this.owner.concat(this.street).concat(String.valueOf(this.number));
	return this.code.concat(String.valueOf(unique.hashCode()));
    }

}

/**
 * File TaskModel.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.model;

import org.json.JSONObject;

import android.util.Log;

/**
 * The Class TaskModel.
 */
public class TaskModel {

    private int id;

    private String title;

    private String createdAt;

    private boolean done;

    private String updated;

    /**
     * Instantiates a new task model.
     */
    public TaskModel() {
	this.id = -1;
	this.title = "";
	this.createdAt = "";
	this.done = false;
	this.updated = "";
    }

    /**
     * Instantiates a new task model.
     * 
     * @param task
     *            the task
     */
    public TaskModel(JSONObject task) {
	this();

	try {
	    this.id = task.getInt("id");
	    this.title = task.getString("title");
	    this.createdAt = task.getString("created_at");
	    this.done = task.getBoolean("done");
	    this.updated = task.getString("updated_at");
	} catch (Exception e) {
	    Log.e(TaskModel.class.getSimpleName(), e.toString());
	}

    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(String createdAt) {
	this.createdAt = createdAt;
    }

    public boolean isDone() {
	return done;
    }

    public void setDone(boolean done) {
	this.done = done;
    }

    public String getUpdated() {
	return updated;
    }

    public void setUpdated(String updated) {
	this.updated = updated;
    }

}

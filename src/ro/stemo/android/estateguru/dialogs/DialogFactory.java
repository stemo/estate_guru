/**
 * File DialogFactory.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.dialogs;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.model.TaskModel;


/**
 * A factory for creating Dialog objects.
 * 
 * @author Stelian Morariu
 */
public class DialogFactory {

    private static final String PLAYSTORE_URI = "market://search?q=pub:Stelian Morariu";
    
    /**
     * Show about alert dialog.
     * 
     * @param context
     *            the context
     */
    public static void showAboutAlertDialog(final Activity context) {
	final View mView = context.getLayoutInflater().inflate(R.layout.dialog_about, null);
	AlertDialog.Builder about = new AlertDialog.Builder(context);

	about.setTitle(context.getString(R.string.title_about));
	about.setView(mView);

	TextView versionTv = (TextView) mView.findViewById(R.id.appVersion);
	versionTv.setText(getAppVersion(context));

	ImageView logo = (ImageView) mView.findViewById(R.id.stemoLogo);
	logo.setOnClickListener(new View.OnClickListener() {
	    
	    @Override
	    public void onClick(View v) {
		final Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(PLAYSTORE_URI));
		context.startActivity(intent);
	    }
	});
	
	about.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		dialog.dismiss();
	    }
	});

	about.show();

    }

    public static void showTaskStatusDialog(Activity context, TaskModel task) {
	final View mView = context.getLayoutInflater().inflate(R.layout.dialog_task, null);
	AlertDialog.Builder status = new AlertDialog.Builder(context);

	status.setTitle(context.getString(R.string.title_dialog_task));
	status.setView(mView);

	ImageView statusImg = (ImageView) mView.findViewById(R.id.statusImage);
	TextView title = (TextView) mView.findViewById(R.id.taskTitle);
	TextView created = (TextView) mView.findViewById(R.id.createdAt);
	TextView updated = (TextView) mView.findViewById(R.id.updatedAt);

	if (task.isDone()) {
	    statusImg.setImageResource(R.drawable.done);
	} else {
	    statusImg.setImageResource(R.drawable.todo);
	}

	title.setText(task.getTitle());
	title.setTag(task.getId());
	created.setText(task.getCreatedAt());
	updated.setText(task.getUpdated());

	status.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		dialog.dismiss();
	    }
	});

	status.show();

    }

    private static CharSequence getAppVersion(Context context) {
	String version = context.getResources().getString(R.string.app_no_value);
	try {
	    PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	    version = pInfo.versionName.concat(".").concat(String.valueOf(pInfo.versionCode));
	} catch (NameNotFoundException e) {
	    Log.e(DialogFactory.class.getSimpleName(), e.toString());
	}

	return version;
    }

}

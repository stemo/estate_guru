/**
 * File TasksAdapter.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.adapters;

import java.util.List;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.model.TaskModel;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * The Class TasksAdapter.
 * 
 * @author Stelian Morariu
 */
public class TasksAdapter extends ArrayAdapter<TaskModel> {

    /**
     * Instantiates a new tasks adapter.
     * 
     * @param context
     *            the context
     * @param tasks
     *            the tasks
     */
    public TasksAdapter(Context context, List<TaskModel> tasks) {
	super(context, R.layout.adapter_tasks_row, tasks);
    }

    /*
     * (non-Javadoc)
     * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	LinearLayout mView;

	if (convertView == null) {
	    mView = new LinearLayout(getContext());
	    String inflater = Context.LAYOUT_INFLATER_SERVICE;
	    LayoutInflater vi;
	    vi = (LayoutInflater) getContext().getSystemService(inflater);
	    vi.inflate(R.layout.adapter_tasks_row, mView, true);
	} else {
	    mView = (LinearLayout) convertView;
	}

	TaskModel item = getItem(position);

	ImageView statusImg = (ImageView) mView.findViewById(R.id.statusImage);
	TextView title = (TextView) mView.findViewById(R.id.taskTitle);

	if (item.isDone()) {
	    statusImg.setImageResource(R.drawable.done);
	} else {
	    statusImg.setImageResource(R.drawable.todo);
	}

	title.setText(item.getTitle());

	return mView;
    }

    /*
     * (non-Javadoc)
     * @see android.widget.BaseAdapter#hasStableIds()
     */
    @Override
    public boolean hasStableIds() {
	return true;
    }

    /*
     * (non-Javadoc)
     * @see android.widget.ArrayAdapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
	return getItem(position).getId();
    }

}

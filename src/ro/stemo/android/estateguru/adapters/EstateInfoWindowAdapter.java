/**
 * File EstateInfoWindowAdapter.java .
 * 
 * Copyright (C) Stelian Morariu 2013 .
 * 
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ro.stemo.android.estateguru.adapters;

import ro.stemo.android.estateguru.R;
import ro.stemo.android.estateguru.model.EstateModel;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

/**
 * The Class EstateInfoWindowAdapter.
 * 
 * @author Stelian Morariu
 */
public class EstateInfoWindowAdapter implements InfoWindowAdapter {

    private Activity activityContext;

    private EstateModel estateModel;

    public EstateInfoWindowAdapter(Activity context) {
	this.activityContext = context;
    }

    public void updateEstateModel(EstateModel newModel) {
	this.estateModel = newModel;
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.GoogleMap.InfoWindowAdapter#getInfoContents(com.google.android.gms.maps.model.Marker)
     */
    @Override
    public View getInfoContents(Marker marker) {
	return getWindowView();
    }

    /*
     * (non-Javadoc)
     * @see com.google.android.gms.maps.GoogleMap.InfoWindowAdapter#getInfoWindow(com.google.android.gms.maps.model.Marker)
     */
    @Override
    public View getInfoWindow(Marker marker) {
	return null;
    }

    private View getWindowView() {
	// Getting view from the layout file info_window_layout
	View mView = this.activityContext.getLayoutInflater().inflate(R.layout.marker_estate_info, null, false);

	// Getting reference to the TextView to set title
	TextView owner = (TextView) mView.findViewById(R.id.estateOwner);
	TextView type = (TextView) mView.findViewById(R.id.estateType);
	TextView price = (TextView) mView.findViewById(R.id.estatePrice);
	TextView address = (TextView) mView.findViewById(R.id.estateAddress);
	TextView hint = (TextView) mView.findViewById(R.id.favoriteHint);

	if (this.estateModel != null) {
	    owner.setText(this.estateModel.getOwner());
	    type.setText(this.estateModel.getType());
	    price.setText(String.valueOf(this.estateModel.getPrice()));
	    address.setText(computeEstateAddress());

	    if (this.estateModel.isPrefered()) {
		owner.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.star_on, 0);
		hint.setText(this.activityContext.getResources().getString(R.string.msg_favorites_remove));
	    } else {
		owner.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.star_off, 0);
		hint.setText(this.activityContext.getResources().getString(R.string.msg_favorites_add));
	    }

	}

	return mView;
    }

    private CharSequence computeEstateAddress() {
	StringBuilder sb = new StringBuilder();
	sb.append(this.estateModel.getNumber());
	sb.append(", ");
	sb.append(this.estateModel.getStreet());
	sb.append(", ");
	sb.append(this.estateModel.getCity());
	sb.append(", ");
	sb.append(this.estateModel.getCountry());

	return sb.toString();
    }
}
